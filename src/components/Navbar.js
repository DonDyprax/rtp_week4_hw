import React from "react"

import "../styles/css/style.css"
import logo from "../assets/week4logo.png"
import { FaHome, FaGamepad, FaQuestionCircle } from "react-icons/fa"

const Navbar = ( {onHomeClick, onGamesClick, onAboutClick} ) => {
  return (
    <div className="navbar">
      <div className="logo">
        <img src={logo} alt="Site Logo" />
      </div>
      <div className="links">
        <ul>
          <li>
            <a href="/" onClick={ (ev) => onHomeClick(ev)}>
              {" "}
              <FaHome /> Home
            </a>
          </li>
          <li>
            <a href="/" onClick={ (ev) => onGamesClick(ev)}>
              {" "}
              <FaGamepad /> Games
            </a>
          </li>
          <li>
            <a href="/" onClick={ (ev) => onAboutClick(ev)}>
              {" "}
              <FaQuestionCircle />
              About
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Navbar;
