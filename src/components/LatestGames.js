import React from 'react'

import "../styles/css/style.css";
import Games from './Games';

const LatestGames = ( {games, setGameId, loading, setPage}) => {
    return (
        <div className='latest-games'>
            <Games games={games} setGameId={setGameId} loading={loading} setPage={setPage} />
        </div>
    )
}

export default LatestGames;
