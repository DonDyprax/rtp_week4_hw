import React, { useState, useEffect } from "react";
import "../styles/css/style.css";

import Pagination from "./Pagination";
import Games from "./Games";

const GamesList = ({ setGameId, setPage }) => {
  const [games, setGames] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [gamesPerPage] = useState(8);

  useEffect(() => {
    const fetchGames = async () => {
      setLoading(true);
      const res = await fetch("http://localhost:8080/games", {
        method: "GET",
        headers: {
          "access-control-allow-origin": "*",
          "Content-type": "application/json; charset=UTF-8",
        },
      });

      const data = await res.json();
      setGames(data);
      setLoading(false);
    };

    fetchGames();
  }, []);

  //Get current games
  const indexOfLastGame = currentPage * gamesPerPage;
  const indexOfFirstGame = indexOfLastGame - gamesPerPage;
  const currentGames = games.slice(indexOfFirstGame, indexOfLastGame);

  //Change page
  const paginate = (pageNumber, event) => {
    event.preventDefault();
    setCurrentPage(pageNumber);
  };

  return (
    <div className="list-container">
      <div className="games-list">
        <Games
          games={currentGames}
          loading={loading}
          setGameId={setGameId}
          setPage={setPage}
        />
      </div>
      <Pagination
        gamesPerPage={gamesPerPage}
        totalGames={games.length}
        paginate={paginate}
      />
    </div>
  );
};

export default GamesList;
