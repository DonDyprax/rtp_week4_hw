import React, {useState, useEffect} from "react";

import { FaCalendarAlt, FaBuilding, FaUserFriends, FaSave, FaGamepad, FaTag, FaBook } from "react-icons/fa";

import "../styles/css/style.css";
import LatestGames from "./LatestGames";

const Home = ({ setGameId, setPage }) => {
    const [latest, setLatest] = useState([]);
    const [loading, setLoading] = useState(false);
    const [newest, setNewest] = useState({});

    const setNewestValue = (value) => {
      setNewest(value);
    }

    const onNewestClick = () => {
      setGameId(newest.id);
      setPage('Game');
    }

    useEffect(() => {
        const fetchGames = async () => {
          setLoading(true);
          const res = await fetch("http://localhost:8080/games", {
            method: "GET",
            headers: {
              "access-control-allow-origin": "*",
              "Content-type": "application/json; charset=UTF-8",
            },
          });
    
          const data = await res.json();
          setLatest(data.slice(-4,));
          setNewestValue(data[data.length - 1]);
          setLoading(false);
        };
    
        fetchGames();
      }, []);

      if(!newest) {
        return <h2>Loading...</h2>
      } else {
        return (
          <div className="home-container">
            <div className="home-content">
              <div className="home-newest">
                <div className="newest-heading">
                  <h1>Newest Release </h1>
                </div>
                <div className="newest-content">
                  <div className="newest-image">
                    <img src={newest.image} alt="Game" onClick={onNewestClick}/>
                  </div>
                  <div className="newest-details">
                    <div className="newest-details-title">
                      <FaBook />
                      <b>Title: </b>
                      {newest.title}
                      </div>
                    <div className="newest-details-date">
                      <FaCalendarAlt />
                      <b>Release Year: </b>
                      {newest.releaseYear}
                      </div>
                    <div className="newest-details-publisher">
                      <FaBuilding />
                      <b>Publisher: </b>
                      {newest.publisher}
                      </div>
                    <div className="newest-details-players">
                      <FaUserFriends />
                      <b>Players: </b>
                      {newest.players}
                      </div>
                    <div className="newest-details-gameSize">
                      <FaSave />
                      <b>Game file size: </b>
                      {newest.gameSize}
                      </div>
                    <div className="newest-details-genres">
                      <FaGamepad />
                      <b>Genres: </b>
                      {newest.genres}
                      </div>
                    <div className="newest-details-price">
                      <FaTag />
                      <b>Price: </b>
                      {newest.price}
                      </div>
                  </div>
                </div>
              </div>
              <div className="home-latest">
                <h2>Latest Games </h2>
                <LatestGames games={latest} loading={loading} setGameId={setGameId} setPage={setPage}/>
              </div>
            </div>
          </div>
        );
      }

  
};

export default Home;
