import React from 'react'

import GameCard from './GameCard'

const Games = ( {games, loading, setGameId, setPage} ) => {
    if(loading) {
        return <h2>Loading...</h2>;
    }

    return (
    <>
        {games.map(game => (
            <GameCard key={game.id} data={game} setGameId={setGameId} setPage={setPage}/>
        ))}
    </>
    )
}

export default Games;
