import React from "react";

const Comment = ({ comment }) => {
  if (!comment) {
    return <h3>Loading...</h3>;
  } else {
    return (
      <div className="comment">
        <h3>{comment.author}</h3>
        <p>{comment.comment}</p>
      </div>
    );
  }
};

export default Comment;
