import React, { useState, useEffect } from "react";

import Comments from './Comments';
import "../styles/css/style.css";
import { FaCalendarAlt, FaBuilding, FaUserFriends, FaSave, FaGamepad, FaTag } from "react-icons/fa";

const Game = ( { id }) => {
  const [data, setData] = useState(null);

  useEffect(() => {
    async function fetchGame() {
      const res = await fetch(`http://localhost:8080/games/${id}`, {
              method: "GET",
              headers: {
                "access-control-allow-origin" : "*",
                "Content-type": "application/json; charset=UTF-8"
              }
          });
      const responseData = await res.json();
      console.log(responseData);
      setData(responseData);
    }

    fetchGame();
  }, [id])

  if(!data) {
    return <h2>Loading...</h2>
  }

  return (
    <div className="game-container">
      <div className="game-content">
        <div className="game-title">
            <h1>{data.title}</h1>
        </div>
        <div className="game-image">
            <img src={data.image} alt="Game"/>
        </div>
        <div className="game-description">{data.description}</div>
        <div className="game-details">
          <div className="details-grid">
            <div className="details-date">
              <FaCalendarAlt />
              <b>Release Year: </b>
              {data.releaseYear}
            </div>
            <div className="details-publisher">
              <FaBuilding />
              <b>Publisher: </b>
              {data.publisher}
            </div>
            <div className="details-players">
              <FaUserFriends />
              <b>Players: </b>
              {data.players}
            </div>
            <div className="details-gamesize">
              <FaSave />
              <b>Game file size: </b>
              {data.gameSize}
            </div>
            <div className="details-genres">
              <FaGamepad />
              <b>Genres: </b>
              {data.genres}
            </div>
            <div className="details-price">
              <FaTag />
              <b>Price: </b>
              {data.price}
            </div>
          </div>
        </div>
        <div className="game-comments">
          <h2>Comments</h2>
          <div className="comments">
            <Comments comments={data.comments}/>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Game;
