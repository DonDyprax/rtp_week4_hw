import React from 'react'

import Comment from './Comment';

const Comments = ({comments}) => {

    if (!comments) {
        return <h3>This post has no comments</h3>
    } else {
        return (
            <>
                {comments.map(comment => (
                <Comment comment={comment}/>
            ))}
            </>
        )
    }
}

export default Comments;