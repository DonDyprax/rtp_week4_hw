import React from "react";

import "../styles/css/style.css";
import { FaCalendarAlt } from "react-icons/fa";

const GameCard = ({ data, setGameId, setPage }) => {

    const onGameCardClick = () => {
        console.log('Game Card Clicked!');
        setGameId(data.id);
        setPage("Game");
    }
  

  return (
    <div className="gamecard" onClick={onGameCardClick}>
      <div className="gamecard-image">
        <img
          src={data.image}
          alt="Game"
        />
      </div>
      <div className="gamecard-details">
        <div className="card-details-date">
          {" "}
          <FaCalendarAlt /> {data.releaseYear}
        </div>
        <div className="card-details-title">{data.title}</div>
        <div className="card-details-price">{data.price}</div>
      </div>
    </div>
  );
};

export default GameCard;
