import React, { useState } from "react";
import "../styles/css/style.css";

import Navbar from "./Navbar";
import GamesList from "./GamesList";
import Game from './Game';
import Home from './Home';
import Footer from './Footer';

function App() {
  const [page, setPage] = useState("Home");
  const [gameId, setGameId] = useState();

  const onHomeClick = (ev) => {
    ev.preventDefault();
    setPage("Home");
  };

  const onAboutClick = (ev) => {
    ev.preventDefault();
    setPage("About");
  };

  const onGamesClick = (ev) => {
    ev.preventDefault();
    setPage("Games");
  }

  const pages = {
    Home: <Home setGameId={setGameId} setPage={setPage}/>,
    Games: <GamesList setGameId={setGameId} setPage={setPage}/>,
    About: <></>,
    Game: <Game id={gameId} />
  };

  return (
    <div className="App">
      <Navbar onHomeClick={onHomeClick} onGamesClick={onGamesClick} onAboutClick={onAboutClick}/>
      {pages[page]}
      <Footer />
    </div>
  );
}

export default App;
